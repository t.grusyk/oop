public class ClassForInterface implements FirstInterface {
//    public void  printName(){
//        System.out.println("Цей метод у окремому класі");
//    }
    String c;

    @Override
    public void printName(int a, int v) {
        double c = a + v;
        System.out.println("@Override printName Int  =  " + c);
    }

    @Override
    public void printName(String a, String v) {
        if (a=="Taras"){
            c = a + v;
            System.out.println("(ClassForInterface) @Override printName String =  " + c);
        }
        else {
            c = v + a;
            System.out.println("(ClassForInterface) @Override printName String =  " + c);
        }
    }
}
