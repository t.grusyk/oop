public class Main {
    public static void main(String[] args) {
        Math math = new Math();
        //Geometry geometry =  new Geometry();
        Geometry figure = new Figure();
        Algebra algebra = new Algebra();
        ExampleAlgebra exampleAlgebra = new ExampleAlgebra();
        ClassForInterface classForInterface = new ClassForInterface();

        math.addTwoNumbers(10,2);
        math.subtractTwoNumbers(10,15);
        math.multiplicTwoNumbers(10,2);
        exampleAlgebra.divisionTwoNumbers(10,0);

        figure.findSquareRectangle(5,10);
        figure.findPerimert(10,5);
        algebra.thirdMethod();
        String[] arr = {};

        classForInterface.printName(2,4);
        classForInterface.printName("Taras", "Java");
    }
}
